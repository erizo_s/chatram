import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatUDP extends JFrame {

    private JTextArea textArea;
    private JTextField textField;

    private final String FRM_TITLE = "Our Tiny Chat";
    private final int FRM_LOCAL_X = 100;
    private final int FRM_LOCAL_Y = 100;
    private final int FRM_WIDTH = 400;
    private final int FRM_HEIGHT = 400;

    private final int PORT = 9876;
    private final String IP_BROADCAST = "192.168.100.255";

    private class theReceiver extends Thread{
        @Override
        public void start(){
            super.start();
            try{
                customize();
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    private void customize() throws Exception{
        DatagramSocket receiverSocket = new DatagramSocket(PORT);
        Pattern regex = Pattern.compile("[\u0020-\uFFFF]");

        while (true){
            byte[] receiveData = new byte[2048];
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            receiverSocket.receive(receivePacket);
//            String name = System.getProperty("user.name");
            InetAddress IPAddres = receivePacket.getAddress();
            int port = receivePacket.getPort();
            String sentence = new String(receivePacket.getData(), StandardCharsets.UTF_8);
            Matcher m = regex.matcher(sentence);
//            textArea.append(IPAddres.toString() + ":" + port + ": " );
//            textArea.append(name + ": ");
            while (m.find())
                textArea.append(sentence.substring(m.start(), m.end()));
            textArea.append("\r\n");
            textArea.setCaretPosition(textArea.getText().length());
        }
    }

    private void buttonSendHandler() throws Exception{
        DatagramSocket sendSocket = new DatagramSocket();
        InetAddress ipAddress = InetAddress.getByName(IP_BROADCAST);
        byte[] sendData;
        String sentence = textField.getText();
        textField.setText(System.getProperty("user.name") + ": ");
        sendData = sentence.getBytes("UTF-8");
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipAddress, PORT);
        sendSocket.send(sendPacket);
    }

    private void frameDraw(JFrame frame){
        textField = new JTextField(System.getProperty("user.name") + ": ");
        textArea = new JTextArea(FRM_HEIGHT/19, 50);
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setLocation(0, 0);
        textArea.setLineWrap(true);
        textArea.setEditable(false);
        JButton buttonSend = new JButton();
        buttonSend.setText("Send");
        buttonSend.setToolTipText("Broadcast a message");
        buttonSend.addActionListener(e -> {
            try {
                buttonSendHandler();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        });
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle(FRM_TITLE);
        frame.setLocation(FRM_LOCAL_X, FRM_LOCAL_Y);
        frame.setSize(FRM_WIDTH, FRM_HEIGHT);
        frame.setResizable(false);
        frame.getContentPane().add(BorderLayout.NORTH, scrollPane);
        frame.getContentPane().add(BorderLayout.CENTER, textField);
        frame.getContentPane().add(BorderLayout.EAST, buttonSend);
        frame.setVisible(true);

    }

    private void antiStatic(){
        frameDraw(new ChatUDP());
        System.out.println("Hello, World!!");
        new theReceiver().start();
    }

    public static void main(String [] args){
        new ChatUDP().antiStatic();
    }

}
